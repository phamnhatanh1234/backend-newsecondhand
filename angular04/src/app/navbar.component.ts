import {Component, OnInit} from '@angular/core'
import { Subscription } from 'rxjs'
import { Observable } from 'rxjs/Rx'
import {CategoriesService} from './services/categories.service'

@Component({
	selector: 'my-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['../assets/scss/navbar.scss'],
})

export class NavbarComponent implements OnInit {
	public categories: any[];
	
	constructor(private categoryService: CategoriesService) {
		// code...
	}

	ngOnInit() {
		this.loadData();
	}

	loadData() {
		this.categoryService.getList().subscribe((response: any) => {
			this.categories = response;
		});
	}
}