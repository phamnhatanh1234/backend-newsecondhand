import { Component, OnInit, Inject } from '@angular/core'
import { Subscription } from 'rxjs'
import { Router, ActivatedRoute } from '@angular/router'
import { LocationService } from './services/location.service'
import { ProductService } from './services/product.service'
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material'
// import { DialogComponent } from './dialog.component'


@Component({
	selector: 'location-map',
	templateUrl: 'locationmap.component.html',
	styleUrls:['../assets/scss/location.scss']
})
export class LocationMapComponent implements OnInit{
	public _id: number;
	public subscription: Subscription;
	public locations: any[];
	public products: any[];
	public locationinfo: any;
	mylat: number;
	mylng: number;

	cameralat: number;
	cameralng: number;

	animal: string;
	name: string;


	constructor(private router: Router, private activatedRoute: ActivatedRoute, private locationService: LocationService,private productService: ProductService, public dialog: MdDialog) {
		// code...
	}

	ngOnInit() {
		this.subscription = this.activatedRoute.params.subscribe(params => {
			this._id = params['id'];
		});
		this.getLocations();
		this.getUserLocation();
	}

	private getLocations(){
		this.locationService.getList().subscribe((data) => {
			this.locations = data;
			console.log(this.locations);
		});
	}

	private getUserLocation() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(position => {
				this.mylat = position.coords.latitude;
				this.mylng = position.coords.longitude;
				this.cameralat = this.mylat;
				this.cameralng = this.mylng;
			});
		}
	}
	
	public updateContent(id: number){
		this.locationService.getSingle(id).subscribe((data) => {
			this.locationinfo = data;
			console.log(this.locationinfo);
			this.cameralat = this.locationinfo.lat;
			this.cameralng = this.locationinfo.lng;
		});
		this.productService.getListByLocationId(id).subscribe((data) => {
			this.products = data;
			console.log(this.products);
		});
	}

	openDialog(id: number): void {
		this.locationService.getSingle(id).subscribe((data) => {
			this.locationinfo = data;
			this.cameralat = this.locationinfo.lat;
			this.cameralng = this.locationinfo.lng;
		});
		this.productService.getListByLocationId(id).subscribe((data) => {
			this.products = data;
			let dialogRef = this.dialog.open(DialogComponent, {
				width: '500px',
				data: this.products,
			});
			dialogRef.afterClosed().subscribe(result => {
				console.log('The dialog was closed');
			});
		});
	}
}

@Component({
	selector: 'my-dialog',
	templateUrl: './dialog.component.html'
})
export class DialogComponent {
	constructor(public dialogRef: MdDialogRef<DialogComponent>, @Inject(MD_DIALOG_DATA) public data: any) {

	}

	onNoClick(): void {
		this.dialogRef.close();
	}
}