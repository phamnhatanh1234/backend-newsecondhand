import {Component, OnInit} from '@angular/core'
import { Subscription } from 'rxjs'
import { Observable } from 'rxjs/Rx'
import { ProductService } from './services/product.service'
import { Router, ActivatedRoute } from '@angular/router'

@Component({
	selector: 'my-home',
	templateUrl: 'home.component.html',
	styleUrls: ['../assets/scss/home.scss']
})
export class HomepageComponent implements OnInit {
	public products: any[];
	public pages: number[];

	constructor(private productService: ProductService, private router: Router, private activatedRoute: ActivatedRoute) { }

	ngOnInit() {
		this.loadData();
	}

	loadData() {
		this.productService.getList().subscribe((response: any) => {
			this.products = response;
		});
	}
}