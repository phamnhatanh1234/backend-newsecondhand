import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { 
	MdButtonModule,
	MdCardModule,
	MdMenuModule,
	MdToolbarModule,
	MdIconModule,
  MdSidenavModule,
  MdTabsModule,
  MdInputModule,
  MdCheckboxModule,
  MdDialogModule,
  MdCoreModule,
  MdExpansionModule
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar.component';
import { HomepageComponent } from './home.component';
import { ProductByCategoriesComponent } from './productbycategories.component';
import { LocationMapComponent, DialogComponent } from './locationmap.component';

import { ProductDetailComponent } from './product-detail.component';
import { ProfileComponent } from './profile.component';
import { LoginComponent } from './login.component';
import { NoutFoundComponent } from './notfound.component';
import { CategoriesService } from './services/categories.service';
import { LocationService } from './services/location.service';


import { ProductService } from './services/product.service';
import { appRoutes } from  './app.routers';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomepageComponent,
    ProductByCategoriesComponent,
    ProductDetailComponent,
    ProfileComponent,
    NoutFoundComponent,
    LoginComponent,
    LocationMapComponent,
    DialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCF-XGMl1XmcSaYt2VbCYnimFDRC2wsEus'
    }),
    MdButtonModule,
  	MdCardModule,
  	MdMenuModule,
  	MdToolbarModule,
  	MdIconModule,
    MdSidenavModule,
    MdTabsModule,
    MdInputModule,
    MdCheckboxModule,
    MdDialogModule,
    CdkTableModule,
    MdCoreModule,
    MdExpansionModule,
    appRoutes
  ],
  providers: [CategoriesService, ProductService, LocationService],
  entryComponents: [DialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }