import {Component, OnInit} from '@angular/core'
import {Router, ActivatedRoute} from '@angular/router';
import {ProductService} from './services/product.service'
import {CategoriesService} from './services/categories.service'
import {Subscription} from 'rxjs/Subscription';

@Component({
	selector: 'my-vehicle',
	templateUrl: './productbycategories.component.html',
	styleUrls: ['../assets/scss/home.scss']
})

export class ProductByCategoriesComponent implements OnInit {
	public  categoriesId: number;
	public  productByCategories: any[];
	public  _pageId: number;
	public errorMessage: string;
	public subscription: Subscription;
	public _id: number;
	public product: any;
	public totalPage: number;
	constructor(private router: Router, private activatedRoute: ActivatedRoute, private productService: ProductService, private categoriesService: CategoriesService) {
		// code...
	}

	ngOnInit(): void {
		this.subscription = this.activatedRoute.params.subscribe(params => {
			this.categoriesId = params['id'];
			this._pageId = 1;
			this.updateContent();
		});
		this.subscription = this.activatedRoute.params.subscribe(params => {
			this._id = params['id'];
		});
		this.productService.getTotalPageByCategoriesId(this._id).subscribe((data) => {
            this.totalPage = data;
        });
		this.productService.GetSingle(this._id).subscribe((data) => {
            this.product = data;
        });
	}

	updateContent(){
		this.productService.getListByCategoriesId(this.categoriesId, this._pageId)
		.subscribe(
			productByCategories => this.productByCategories = productByCategories,
			error => this.errorMessage = <any>error);
	}

	nextPage(){
		if(this._pageId<this.totalPage){
			this._pageId++;
			this.updateContent();
		};
	}


	previousPage(){
		if(this._pageId>1){
			this._pageId--;
			this.updateContent();
		};
	}

}