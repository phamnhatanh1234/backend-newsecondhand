import { Routes, RouterModule } from '@angular/router'
import { HomepageComponent } from './home.component'
import { ProductByCategoriesComponent } from './productbycategories.component'
import { NavbarComponent } from './navbar.component'
import { NotFoundComponent } from './notfound.component'
import { ProductDetailComponent } from './product-detail.component'
import { ProfileComponent } from './profile.component';
import { LoginComponent } from './login.component';
import { LocationMapComponent } from './locationmap.component';

const routing: Routes = [
	{ path: '', component: LocationMapComponent},
	{ path: 'categories/:id', component: ProductByCategoriesComponent },
	{ path: 'product/:id', component: ProductDetailComponent },
	{ path: '**', component: NoutFoundComponent}
]

export const appRoutes = RouterModule.forRoot(routing);