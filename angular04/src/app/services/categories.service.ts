import { Injectable, Inject } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

export class Category {
	id: number;
	Name: string;
}

@Injectable()
export class CategoriesService {
	private apiUrl = "http://localhost:8081/api/categories";

	constructor(@Inject(Http) private _http: Http) {

	}

	getList(): Observable<Category[]> {
		return this._http.get(this.apiUrl).map((response: Response) => response.json());
	}
}