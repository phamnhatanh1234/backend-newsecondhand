import { Injectable, Inject } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductService {
	private apiUrl = "http://localhost:8081/api/product";

	constructor(@Inject(Http) private _http: Http) {

	}

	getList(): Observable<any[]> {
		return this._http.get(this.apiUrl).map((response: Response) => response.json());
	}

	getListByCategoriesId(categoryId: number, pageId: number): Observable<any[]> {
		return this._http.get(this.apiUrl + `/categories` + `/${categoryId}/` + `page` + `/${pageId}`)
		.map(res => res.json());
	}

	getListByLocationId(locationId: number): Observable<any[]> {
		return this._http.get(this.apiUrl + `/location` + `/${locationId}/`)
		.map(res => res.json());
	}

	getTotalPageByCategoriesId(categoryId: number): Observable<number> {
		return this._http.get(this.apiUrl + `/categories/countpage/` + `/${categoryId}/`)
		.map(res => res.json());
	}


	GetSingle(id: number): Observable<any> {
		return this._http.get(this.apiUrl + "/" + id).map((response: Response) => response.json());
	}

	GetSingleByCategoriesId(id: number, categoryId: number, pageId: number): Observable<any> {
		return this._http.get(this.apiUrl + `/categories` + `/${categoryId}/` + `page` + `/${pageId}` + "/" + id).map((response: Response) => response.json());
	}
}