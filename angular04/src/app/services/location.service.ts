import { Injectable, Inject } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class LocationService {
	private apiUrl = "http://localhost:8081/api/location";

	constructor(@Inject(Http) private _http: Http) {

	}

	getList(): Observable<any[]> {
		return this._http.get(this.apiUrl).map((response: Response) => response.json());
	}

	getSingle(id: number): Observable<any> {
		return this._http.get(this.apiUrl+"/"+id).map((response: Response) => response.json());
	}


}