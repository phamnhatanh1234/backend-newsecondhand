package com.newsecondhand.common;

/**
 * Created by Bi on 6/10/2017.
 */
public class ErrorResponse {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
