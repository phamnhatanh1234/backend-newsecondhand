package com.newsecondhand.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.newsecondhand.entity.Categories;
import com.newsecondhand.entity.Exchange;
import com.newsecondhand.entity.LocationGPS;
import com.newsecondhand.entity.StatusExchange;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by Bi on 5/9/2017.
 */
public class ProductDTO {
    private Long id;
    private String name;
    private String description;
    private String note;
    private Date datecreated;
    private Date dateexpired;
    private String image;
    private StatusExchange status;
    private Long idLocation;
    private double longtitude;
    private double latitude;
    private String address;
    private Long idcategory;
    private String iconUrlCategories;
    private String iconUrlMarker;

    private String categoriesName;
    private String userCreated;
    private String phone;

    public Long getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(Long idLocation) {
        this.idLocation = idLocation;
    }

    public Long getIdcategory() {
        return idcategory;
    }

    public void setIdcategory(Long idcategory) {
        this.idcategory = idcategory;
    }

    public String getIconUrlCategories() {
        return iconUrlCategories;
    }

    public void setIconUrlCategories(String iconUrlCategories) {
        this.iconUrlCategories = iconUrlCategories;
    }

    public String getIconUrlMarker() {
        return iconUrlMarker;
    }

    public void setIconUrlMarker(String iconUrlMarker) {
        this.iconUrlMarker = iconUrlMarker;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }

    public Date getDateexpired() {
        return dateexpired;
    }

    public void setDateexpired(Date dateexpired) {
        this.dateexpired = dateexpired;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public StatusExchange getStatus() {
        return status;
    }

    public void setStatus(StatusExchange status) {
        this.status = status;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCategoriesName() {
        return categoriesName;
    }

    public void setCategoriesName(String categoriesName) {
        this.categoriesName = categoriesName;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
