package com.newsecondhand.dto;

import com.newsecondhand.entity.StatusExchange;

import java.sql.Date;

/**
 * Created by Bi on 26/11/2017.
 */
public class ExchangeDTO {
    private Long idExchange;
    private Long idProduct;
    private String userGiver;
    private String userReceiver;



    private String productName;
    private Date datecreated;
    private StatusExchange statusExchange;

    public Long getIdExchange() {
        return idExchange;
    }

    public void setIdExchange(Long idExchange) {
        this.idExchange = idExchange;
    }

    public Long getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Long idProduct) {
        this.idProduct = idProduct;
    }

    public String getUserGiver() {
        return userGiver;
    }

    public void setUserGiver(String userGiver) {
        this.userGiver = userGiver;
    }

    public String getUserReceiver() {
        return userReceiver;
    }

    public void setUserReceiver(String userReceiver) {
        this.userReceiver = userReceiver;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Date getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }

    public StatusExchange getStatusExchange() {
        return statusExchange;
    }

    public void setStatusExchange(StatusExchange statusExchange) {
        this.statusExchange = statusExchange;
    }
}
