package com.newsecondhand.repository;

import com.newsecondhand.entity.LocationGPS;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Bi on 11/9/2017.
 */
@Repository
public interface LocationGPSRepository extends CrudRepository<LocationGPS, Long> {


    @Query(value = "SELECT * FROM locationgps WHERE locationgps.usercreated = :username", nativeQuery = true)
    List<LocationGPS> getLocationsByUsername(@Param("username") String username);

}
