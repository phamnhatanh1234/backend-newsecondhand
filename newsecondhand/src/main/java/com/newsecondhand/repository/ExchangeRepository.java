package com.newsecondhand.repository;

import com.newsecondhand.entity.Exchange;
import com.newsecondhand.entity.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Bi on 26/11/2017.
 */
@Repository
public interface ExchangeRepository extends CrudRepository<Exchange, Long>{
    @Query(value = "SELECT * FROM exchange WHERE product_id = :product_id AND status_exchange <1", nativeQuery = true)
    Exchange getPendingExchangeByProductId(@Param("product_id") Long product_id);

    @Query(value = "SELECT * FROM exchange WHERE product_id = :product_id", nativeQuery = true)
    Exchange getExchangeByProductId(@Param("product_id") Long product_id);
}
