package com.newsecondhand.repository;

import com.newsecondhand.entity.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Bi on 5/9/2017.
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    @Query(value = "SELECT * FROM product WHERE product.id NOT IN (SELECT product_id From exchange WHERE status_exchange <= 1)", nativeQuery = true)
    List<Product> getProductListByAvailable();

    @Query(value = "SELECT * FROM product WHERE product.id NOT IN (SELECT product_id From exchange WHERE status_exchange <= 1) AND product.category_id = :categoryid", nativeQuery = true)
    List<Product> getProductByCategories(@Param("categoryid") long categoryid);


    @Query(value = "SELECT * FROM product WHERE product.id NOT IN (SELECT product_id From exchange WHERE status_exchange <= 1) AND product.category_id = :categoryid LIMIT :location,:size", nativeQuery = true)
    List<Product> getProductByCategoriesWithPaging(@Param("categoryid") long categoryid, @Param("location") long location, @Param("size") long size);


    @Query(value = "SELECT CEILING(COUNT(*)/:size) FROM product  WHERE product.category_id = :categoryid", nativeQuery = true)
    int countPageOfListProductByCategoryId(@Param("categoryid") long categoryid, @Param("size") int size);


    @Query(value = "SELECT * FROM product " +
            "WHERE product.id NOT IN (SELECT product_id From exchange WHERE status_exchange <= 1)" +
            "ORDER BY product.datecreated DESC LIMIT :location,:size", nativeQuery = true)
    List<Product> getListNewProductWithPaging( @Param("location") long location, @Param("size") long size);

    @Query(value = "SELECT CEILING(COUNT(*)/:size) FROM product ", nativeQuery = true)
    int countPageOfListProducts( @Param("size") int size);



    @Query(value = "SELECT * FROM product WHERE product.gps_id = :locationid " +
            "AND product.id NOT IN (SELECT product_id From exchange WHERE status_exchange <= 1)", nativeQuery = true)
    List<Product> getListProductByLocation(@Param("locationid") long locationid);


    @Query(value = "SELECT * FROM product WHERE product.gps_id IN (SELECT locationgps.id FROM locationgps WHERE username LIKE :username) " , nativeQuery = true)
    List<Product> getListProductByUserCreated(@Param("username") String username);

    @Query(value = "SELECT * FROM product WHERE product.id IN (SELECT product_id FROM exchange WHERE user_receiver LIKE :username ORDER Y status_exchange) " , nativeQuery = true)
    List<Product> getListProductByUserReceiver(@Param("username") String username);
}
