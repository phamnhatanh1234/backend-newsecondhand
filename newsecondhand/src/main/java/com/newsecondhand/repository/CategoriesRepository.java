package com.newsecondhand.repository;

import com.newsecondhand.entity.Categories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Bi on 5/9/2017.
 */
@Repository
public interface CategoriesRepository extends CrudRepository<Categories, Long> {
}
