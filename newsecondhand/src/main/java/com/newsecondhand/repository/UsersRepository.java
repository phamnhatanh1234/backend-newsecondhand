package com.newsecondhand.repository;

import com.newsecondhand.entity.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Bi on 7/9/2017.
 */
@Repository
public interface UsersRepository extends CrudRepository<Users, String> {


}
