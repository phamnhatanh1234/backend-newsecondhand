package com.newsecondhand.service;

import com.newsecondhand.entity.Categories;
import com.newsecondhand.repository.CategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bi on 5/9/2017.
 */

@Service
@Transactional
public class CategoryServiceImpl implements CategoriesService {

    @Autowired
    private CategoriesRepository categoriesRepository;

    @Override
    public Categories createNewCategory(Categories categories) {

        return categoriesRepository.save(categories);
    }

    @Override
    public Categories updateCategory(long categoryId, Categories categories) {
        categories.setId(categoryId);
        return categoriesRepository.save(categories);
    }

    @Override
    public void deleteCategory(long categoriesId) {
        categoriesRepository.delete(categoriesId);
    }

    @Override
    public Categories findCategoryById(long categoryid) {
        return categoriesRepository.findOne(categoryid);
    }

    @Override
    public List<Categories> getListCategories() {
        List<Categories> categories = new ArrayList<>();
        categoriesRepository.findAll().forEach(categories::add);
        return categories;
    }
}
