package com.newsecondhand.service;

import com.newsecondhand.dto.ExchangeDTO;
import com.newsecondhand.entity.Exchange;
import com.newsecondhand.entity.Product;
import com.newsecondhand.entity.StatusExchange;
import com.newsecondhand.entity.Users;
import com.newsecondhand.repository.ExchangeRepository;
import com.newsecondhand.repository.ProductRepository;
import com.newsecondhand.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Bi on 6/12/2017.
 */
@Service
public class ExchangeServiceImpl implements ExchangeService {

    @Autowired
    ExchangeRepository exchangeRepository;
    @Autowired
    ProductRepository productRepository;

    @Autowired
    UsersRepository usersRepository;

    private ExchangeDTO tranferEntityToDTO(Exchange item){
        ExchangeDTO itemDTO = new ExchangeDTO();
        itemDTO.setIdExchange(item.getId());
        itemDTO.setIdProduct(item.getProduct().getId());
        itemDTO.setUserReceiver(item.getUserReceiver().getUsername());
        itemDTO.setUserGiver(item.getUserGiver().getUsername());
        itemDTO.setStatusExchange(item.getStatusExchange());
        itemDTO.setDatecreated(item.getDatecreated());
        itemDTO.setProductName(item.getProduct().getName());

        return itemDTO;
    }

    private Exchange transferDTOtoEntity(ExchangeDTO itemDTO){
        Exchange item = new Exchange();
        item.setId(itemDTO.getIdExchange());
        Product product = productRepository.findOne(itemDTO.getIdProduct());
        item.setProduct(product);
        Users usersGiver = usersRepository.findOne(itemDTO.getUserGiver());
        item.setUserGiver(usersGiver);
        Users usersReceiver = null;
        if(!itemDTO.getUserReceiver().isEmpty()){
            usersReceiver = usersRepository.findOne(itemDTO.getUserReceiver());
        }else {
            usersReceiver = product.getLocation().getUserCreated();
        }
        item.setUserReceiver(usersReceiver);
        item.setDatecreated(itemDTO.getDatecreated());
        item.setStatusExchange(itemDTO.getStatusExchange());
        return item;
    }

    @Override
    public ExchangeDTO createExchange(ExchangeDTO itemDTO) {
        Exchange item = transferDTOtoEntity(itemDTO);
        item.setStatusExchange(StatusExchange.Pending);
        item = exchangeRepository.save(item);
        ExchangeDTO itemNew = tranferEntityToDTO(item);
        return itemNew;
    }

    @Override
    public ExchangeDTO completeExchange(Long idproduct) {
        Exchange item = exchangeRepository.getPendingExchangeByProductId(idproduct);
        item.setStatusExchange(StatusExchange.Completed);
        item = exchangeRepository.save(item);
        ExchangeDTO itemDTO = tranferEntityToDTO(item);
        return itemDTO;
    }

    @Override
    public ExchangeDTO canceledExchange(Long idproduct) {
        Exchange item = exchangeRepository.getPendingExchangeByProductId(idproduct);
        ExchangeDTO itemDTO = tranferEntityToDTO(item);
        exchangeRepository.delete(item);
        return itemDTO;
    }

    @Override
    public boolean isExistPendingExchangeByProductId(Long idproduct) {
        Exchange item = exchangeRepository.getPendingExchangeByProductId(idproduct);
        if(item == null) return false;
        return true;
    }
}
