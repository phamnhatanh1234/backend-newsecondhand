package com.newsecondhand.service;

import com.newsecondhand.dto.LocationDTO;
import com.newsecondhand.entity.LocationGPS;
import com.newsecondhand.entity.Users;
import com.newsecondhand.repository.LocationGPSRepository;
import com.newsecondhand.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bi on 11/9/2017.
 */
@Service
public class LocationGPSServiceImpl implements LocationGPSSevice{

    @Autowired
    private LocationGPSRepository locationGPSRepository;

    @Autowired
    private UsersRepository usersRepository;

    private LocationDTO tranferEntityToDTO(LocationGPS item){
        LocationDTO itemDTO = new LocationDTO();
        itemDTO.setId(item.getId());
        itemDTO.setLatitude(item.getLatitude());
        itemDTO.setLongtitude(item.getLongtitude());
        itemDTO.setAddress(item.getAddress());
        itemDTO.setUserCreated(item.getUserCreated().getUsername());
        itemDTO.setUserfullname(item.getUserCreated().getFullname());
        itemDTO.setPhone(item.getUserCreated().getPhone());
        itemDTO.setDateCreated(item.getDateCreated());
        return itemDTO;
    }

    private LocationGPS tranferDTOToEntity(LocationDTO itemDTO){
        LocationGPS item = new LocationGPS();
        item.setId(itemDTO.getId());
        item.setAddress(itemDTO.getAddress());
        item.setLatitude(itemDTO.getLatitude());
        item.setLongtitude(itemDTO.getLongtitude());
        item.setDateCreated(item.getDateCreated());
        Users users = usersRepository.findOne(itemDTO.getUserCreated());
        item.setUserCreated(users);
        return item;
    }


    @Override
    public LocationGPS createNewLocation(LocationDTO itemDTO) {
        LocationGPS item = tranferDTOToEntity(itemDTO);

        return locationGPSRepository.save(item);
    }

    @Override
    public LocationGPS updateLocation(long id, LocationGPS locationGPS) {
        locationGPS.setId(id);
        return locationGPSRepository.save(locationGPS);
    }

    @Override
    public void deleteLocation(long id) {
        locationGPSRepository.delete(id);
    }

    @Override
    public LocationDTO findLocationById(long locationid) {

        LocationGPS repo =  locationGPSRepository.findOne(locationid);
        LocationDTO item = tranferEntityToDTO(repo);
        return item;
    }

    @Override
    public List<LocationDTO> getListLocationGPS() {

        List<LocationGPS> list = new ArrayList<>();
        locationGPSRepository.findAll().forEach(list::add);
        List<LocationDTO> listDTO = new ArrayList<>();
        for(LocationGPS item: list){
            LocationDTO itemDTO = tranferEntityToDTO(item);
            listDTO.add(itemDTO);
        }
        return listDTO;
    }

    @Override
    public List<LocationDTO> getListLocationGPSByUsername(String username) {
        List<LocationGPS> list = new ArrayList<>();
        locationGPSRepository.getLocationsByUsername(username);
        List<LocationDTO> listDTO = new ArrayList<>();
        for(LocationGPS item: list){
            LocationDTO itemDTO = tranferEntityToDTO(item);
            listDTO.add(itemDTO);
        }
        return listDTO;
    }
}
