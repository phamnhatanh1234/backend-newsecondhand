package com.newsecondhand.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.newsecondhand.dto.ProductDTO;
import com.newsecondhand.entity.*;
import com.newsecondhand.repository.*;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Bi on 5/9/2017.
 */
@Service
@Transactional
public class ProductServiceImpl implements   ProductService{

    final int PRODUCT_LIST_SIZE_BY_CATEGORY = 9;
    final int PRODUCT_NEW_LIST_SIZE = 9;

    @Autowired
    private ProductRepository productRepository;


    @Autowired
    private LocationGPSRepository locationGPSRepository;
    @Autowired
    private CategoriesRepository categoriesRepository;

    @Autowired
    private ExchangeRepository exchangeRepository;





    @Override
    public List<ProductDTO> getListProductsByCategoryIdWithPaging(Long categoriesId, int pageId) {
        int location = (pageId-1)*PRODUCT_LIST_SIZE_BY_CATEGORY;
        List<Product> products = productRepository.getProductByCategoriesWithPaging(categoriesId, location, PRODUCT_LIST_SIZE_BY_CATEGORY);
        List<ProductDTO> productDTOlist = new ArrayList<>();
        int size = products.size();
        for(Product item: products){
            ProductDTO itemDTO = tranferEntityToDTO(item);
            productDTOlist.add(itemDTO);
        }
        return productDTOlist;
    }

    @Override
    public int getTotalPageOfProductsByCategoryWithPaging(Long categoriesId) {
        return productRepository.countPageOfListProductByCategoryId(categoriesId, PRODUCT_LIST_SIZE_BY_CATEGORY);
    }

    @Override
    public ProductDTO findProductById(long productid) {
        Product find = productRepository.findOne(productid);
        ProductDTO found = tranferEntityToDTO(find);
        return found;
    }

    @Override
    public List<Product> getListAvailableProductsByLocationGPSID(Long locationGPSId) {
        return null;
    }

    @Override
    public Product createNewProduct(String product, MultipartFile file)  throws IOException {
       return null;
    }

    @Override
    public Product updateProduct(long productId, Product product) {
        return null;
    }

    @Override
    public void deleteProduct(long productId) {
    }


    private ProductDTO tranferEntityToDTO(Product item){
        ProductDTO itemDTO = new ProductDTO();
        itemDTO.setId(item.getId());
        itemDTO.setName(item.getName());
        itemDTO.setDescription(item.getDescription());
        itemDTO.setNote(item.getNote());
        itemDTO.setDatecreated(item.getDatecreated());
        itemDTO.setDateexpired(item.getDateexpired());
        itemDTO.setImage(item.getImage());
        //Check lại đoạn này
        Exchange exchange = exchangeRepository.getExchangeByProductId(item.getId());
        if(exchange == null) {
            itemDTO.setStatus(StatusExchange.Available);
        }else{
            itemDTO.setStatus(exchange.getStatusExchange());
        }
        itemDTO.setIdLocation(item.getLocation().getId());
        itemDTO.setLongtitude(item.getLocation().getLongtitude());
        itemDTO.setLatitude(item.getLocation().getLatitude());
        itemDTO.setAddress(item.getLocation().getAddress());
        itemDTO.setUserCreated(item.getLocation().getUserCreated().getUsername());
        itemDTO.setPhone(item.getLocation().getUserCreated().getPhone());
        itemDTO.setIdcategory(item.getCategories().getId());
        itemDTO.setCategoriesName(item.getCategories().getName());
        return itemDTO;
    }

    private Product transferDTOtoEntity(ProductDTO itemDTO){
        Product item = new Product();
        item.setId(itemDTO.getId());
        item.setName(itemDTO.getName());
        item.setDescription(itemDTO.getDescription());
        item.setDatecreated(itemDTO.getDatecreated());
        item.setDateexpired(itemDTO.getDateexpired());
        item.setImage(itemDTO.getImage());
        Categories categories = categoriesRepository.findOne(itemDTO.getIdcategory());
        item.setCategories(categories);
        LocationGPS locationGPS = locationGPSRepository.findOne(itemDTO.getIdLocation());
        item.setLocation(locationGPS);
        return item;
    }

    @Override
    public List<ProductDTO> getAllProducts() {
        List<Product> products = productRepository.getProductListByAvailable();
        List<ProductDTO> productDTOlist = new ArrayList<>();
        int size = products.size();
        for(Product item: products){
            ProductDTO itemDTO = tranferEntityToDTO(item);
            productDTOlist.add(itemDTO);
        }
        return productDTOlist;
    }

    @Override
    public List<ProductDTO> getAllProductsByCategoriesId(int categoriesId) {
        List<Product> products = productRepository.getProductByCategories(categoriesId);
        productRepository.findAll().forEach(products::add);

        List<ProductDTO> productDTOlist = new ArrayList<>();
        int size = products.size();
        for(Product item: products){
            ProductDTO itemDTO = tranferEntityToDTO(item);
            productDTOlist.add(itemDTO);
        }
        return productDTOlist;
    }

    @Override
    public List<ProductDTO> getProductsByLocations(long locationId) {
        List<Product> products = productRepository.getListProductByLocation(locationId);
        List<ProductDTO> productDTOlist = new ArrayList<>();
        int size = products.size();
        for(Product item: products){
            ProductDTO itemDTO = tranferEntityToDTO(item);
            productDTOlist.add(itemDTO);
        }
        return productDTOlist;
    }

    @Override
    public List<ProductDTO> getListNewProductsWithPaging(int pageId) {
        int location = (pageId-1)*PRODUCT_NEW_LIST_SIZE;
        List<Product> products = productRepository.getListNewProductWithPaging(location, PRODUCT_NEW_LIST_SIZE);
        List<ProductDTO> productDTOlist = new ArrayList<>();
        int size = products.size();
        for(Product item: products){
            ProductDTO itemDTO = tranferEntityToDTO(item);
            productDTOlist.add(itemDTO);
        }
        return productDTOlist;
    }

    @Override
    public int getTotalPageOfNewProductsWithPaging() {
        return productRepository.countPageOfListProducts(PRODUCT_NEW_LIST_SIZE);
    }

    @Override
    public List<ProductDTO> getProductsByUserCreated(String username) {
        List<Product> products = productRepository.getListProductByUserCreated(username);
        List<ProductDTO> productDTOlist = new ArrayList<>();
        int size = products.size();
        for(Product item: products){
            ProductDTO itemDTO = tranferEntityToDTO(item);
            productDTOlist.add(itemDTO);
        }
        return productDTOlist;
    }

    @Override
    public List<ProductDTO> getProductsByUserReceiver(String username) {
        List<Product> products = productRepository.getListProductByUserReceiver(username);
        List<ProductDTO> productDTOlist = new ArrayList<>();
        int size = products.size();
        for(Product item: products){
            ProductDTO itemDTO = tranferEntityToDTO(item);
            productDTOlist.add(itemDTO);
        }
        return productDTOlist;
    }
}
