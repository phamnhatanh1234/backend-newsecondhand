package com.newsecondhand.service;

import com.newsecondhand.entity.Categories;

import java.util.List;

/**
 * Created by Bi on 5/9/2017.
 */
public interface CategoriesService {

    Categories createNewCategory(Categories categories);

    Categories updateCategory(long categoryId, Categories categories);

    void deleteCategory(long categoriesId);

    Categories findCategoryById(long categoryid);

    List<Categories> getListCategories();
}
