package com.newsecondhand.service;

import com.newsecondhand.dto.ExchangeDTO;

/**
 * Created by Bi on 26/11/2017.
 */
public interface ExchangeService {

    public ExchangeDTO createExchange(ExchangeDTO itemDTO);

    public ExchangeDTO completeExchange(Long idproduct);

    public ExchangeDTO canceledExchange(Long idproduct);


    public boolean isExistPendingExchangeByProductId(Long idproduct);
}
