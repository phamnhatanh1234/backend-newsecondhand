package com.newsecondhand.service;
import com.newsecondhand.dto.UserDTO;
import com.newsecondhand.entity.Users;
import java.util.*;
/**
 * Created by Bi on 7/9/2017.
 */

public interface UsersService {
    UserDTO createNewUsers(Users users);

    UserDTO updateUser(String username, Users users);

    void deleteUser(String username);

    UserDTO findUserByUsername(String username);

    List<UserDTO> getListUsers();
}
