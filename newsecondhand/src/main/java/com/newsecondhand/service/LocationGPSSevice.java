package com.newsecondhand.service;

import com.newsecondhand.dto.LocationDTO;
import com.newsecondhand.entity.LocationGPS;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Bi on 11/9/2017.
 */
public interface LocationGPSSevice {

    LocationGPS createNewLocation(LocationDTO item);

    LocationGPS updateLocation(long locationId, LocationGPS item);

    void deleteLocation(long locationId);

    LocationDTO findLocationById(long locationId);

    List<LocationDTO> getListLocationGPS();

    List<LocationDTO> getListLocationGPSByUsername(String username);

}
