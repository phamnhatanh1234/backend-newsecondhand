package com.newsecondhand.service;

import com.newsecondhand.dto.ProductDTO;
import com.newsecondhand.entity.Product;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by Bi on 5/9/2017.
 */

public interface ProductService {

    List<ProductDTO> getListProductsByCategoryIdWithPaging(Long categoriesId, int pageId);

    int getTotalPageOfProductsByCategoryWithPaging(Long categoriesId);

    ProductDTO findProductById(long categoryid);

    List<Product> getListAvailableProductsByLocationGPSID(Long locationGPSId);

    Product createNewProduct(String product, MultipartFile file) throws IOException;

    Product updateProduct(long productId, Product product);

    void deleteProduct(long productId);

    List<ProductDTO> getAllProducts();

    List<ProductDTO> getAllProductsByCategoriesId(int categoriesId);

    List<ProductDTO> getProductsByLocations(long locationId);

    List<ProductDTO> getListNewProductsWithPaging(int pageId);

    int getTotalPageOfNewProductsWithPaging();


    List<ProductDTO> getProductsByUserCreated(String username);

    List<ProductDTO> getProductsByUserReceiver(String username);


}
