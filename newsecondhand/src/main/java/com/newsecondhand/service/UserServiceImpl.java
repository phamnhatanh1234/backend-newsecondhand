package com.newsecondhand.service;

import com.newsecondhand.dto.UserDTO;
import com.newsecondhand.entity.Role;
import com.newsecondhand.entity.Users;
import com.newsecondhand.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Bi on 7/9/2017.
 */
@Service
@Transactional
public class UserServiceImpl implements UsersService {




    @Autowired
    private UsersRepository usersRepository;

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    public UserDTO createNewUsers(Users users) {
        Users item = usersRepository.findOne(users.getUsername());
        UserDTO itemDTO = null;
        if(item == null){
            users.setPassword(getPasswordEncoder().encode(users.getPassword()));
            itemDTO = tranferEntityToDTO(usersRepository.save(users));
        }
        return itemDTO;
    }

    @Override
    public UserDTO updateUser(String username, Users users) {
        users.setUsername(username);
        if(users.getPassword()!=null)
            users.setPassword(getPasswordEncoder().encode(users.getPassword()));
        UserDTO itemDTO = tranferEntityToDTO(usersRepository.save(users));
        return itemDTO;
    }



    @Override
    public void deleteUser(String username) {
        usersRepository.delete(username);
    }

    @Override
    public UserDTO findUserByUsername(String username) {
        Users item = usersRepository.findOne(username);
        UserDTO itemDTO = tranferEntityToDTO(item);
        return itemDTO;
    }

    @Override
    public List<UserDTO> getListUsers() {
        List<Users> usersList = new ArrayList<>();
        usersRepository.findAll().forEach(usersList::add);

        List<UserDTO> userDTOlist = new ArrayList<>();

        for(Users item: usersList){
            UserDTO itemDTO = tranferEntityToDTO(item);
            userDTOlist.add(itemDTO);
        }
        return userDTOlist;
    }


    private UserDTO tranferEntityToDTO(Users item){
        UserDTO itemDTO = new UserDTO();
        itemDTO.setUsername(item.getUsername());
        itemDTO.setEmail(item.getEmail());
        itemDTO.setFullname(item.getFullname());
        itemDTO.setPhone(item.getPhone());
        itemDTO.setDatecreated(item.getDatecreated());
        itemDTO.setDateupdated(item.getDateupdated());
        itemDTO.setIdentitycard(item.getIdentitycard());
        return itemDTO;
    }
}
