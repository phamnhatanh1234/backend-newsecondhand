package com.newsecondhand.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Bi on 4/9/2017.
 */
@Entity
public class Categories {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String iconUrlCategories;
    private String iconUrlMarker;



    @OneToMany(mappedBy = "categories", cascade = CascadeType.ALL)
    @JsonBackReference(value = "product")
    private Set<Product> product;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUrlCategories() {
        return iconUrlCategories;
    }

    public void setIconUrlCategories(String iconUrlCategories) {
        this.iconUrlCategories = iconUrlCategories;
    }

    public String getIconUrlMarker() {
        return iconUrlMarker;
    }

    public void setIconUrlMarker(String iconUrlMarker) {
        this.iconUrlMarker = iconUrlMarker;
    }

    public Set<Product> getProduct() {
        return product;
    }

    public void setProduct(Set<Product> product) {
        this.product = product;
    }
}
