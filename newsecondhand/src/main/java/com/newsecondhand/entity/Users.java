package com.newsecondhand.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by Bi on 4/9/2017.
 */
@Entity
public class Users {
    @Id
    @Column(name="username")
    private String username;
    private String password;
    private String fullname;
    private String avatar;
    private String email;
    private String phone;
    private String identitycard;
    private Date datecreated;
    private Date dateupdated;

    @OneToMany(mappedBy = "userCreated", cascade = CascadeType.ALL)
    @JsonBackReference(value = "locationgps")
    private Set<LocationGPS> locationGPSS;

    @OneToMany(mappedBy = "userReceiver", cascade = CascadeType.ALL)
    @JsonBackReference(value = "userreceiver")
    private Set<Exchange> ExchangeReceiver;

    @OneToMany(mappedBy = "userGiver", cascade = CascadeType.ALL)
    @JsonBackReference(value = "usergiver")
    private Set<Exchange> ExchangeGiver;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdentitycard() {
        return identitycard;
    }

    public void setIdentitycard(String identitycard) {
        this.identitycard = identitycard;
    }

    public Date getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }

    public Date getDateupdated() {
        return dateupdated;
    }

    public void setDateupdated(Date dateupdated) {
        this.dateupdated = dateupdated;
    }

    public Set<LocationGPS> getLocationGPSS() {
        return locationGPSS;
    }

    public void setLocationGPSS(Set<LocationGPS> locationGPSS) {
        this.locationGPSS = locationGPSS;
    }



    public Set<Exchange> getExchangeReceiver() {
        return ExchangeReceiver;
    }

    public void setExchangeReceiver(Set<Exchange> exchangeReceiver) {
        ExchangeReceiver = exchangeReceiver;
    }

    public Set<Exchange> getExchangeGiver() {
        return ExchangeGiver;
    }

    public void setExchangeGiver(Set<Exchange> exchangeGiver) {
        ExchangeGiver = exchangeGiver;
    }
}
