package com.newsecondhand.entity;

/**
 * Created by Bi on 4/9/2017.
 */
public enum StatusExchange {
    Pending, Completed, Cancelled, Available
}
