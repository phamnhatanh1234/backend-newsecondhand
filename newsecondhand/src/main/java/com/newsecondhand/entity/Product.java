package com.newsecondhand.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.newsecondhand.entity.Categories;
import com.newsecondhand.entity.Exchange;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by Bi on 4/9/2017.
 */
@Entity
public class Product {
    @Id
    @GeneratedValue

    private Long id;

    private String name;

    @Lob
    @Column(length = 10000)
    private String description;

    private Date datecreated;

    private Date dateexpired;
    private String image;
    @Lob
    @Column(length = 10000)
    private String note;

    @ManyToOne
    @JoinColumn(name = "categoryId")
    private Categories categories;

    @ManyToOne
    @JoinColumn(name = "gpsId")
    private LocationGPS location;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    @JsonBackReference(value = "exchange")
    private Set<Exchange> exchange;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }

    public Date getDateexpired() {
        return dateexpired;
    }

    public void setDateexpired(Date dateexpired) {
        this.dateexpired = dateexpired;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }

    public LocationGPS getLocation() {
        return location;
    }

    public void setLocation(LocationGPS location) {
        this.location = location;
    }

    public Set<Exchange> getExchange() {
        return exchange;
    }

    public void setExchange(Set<Exchange> exchange) {
        this.exchange = exchange;
    }
}
