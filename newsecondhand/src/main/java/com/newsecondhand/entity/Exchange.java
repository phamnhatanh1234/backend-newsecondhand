package com.newsecondhand.entity;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Bi on 4/9/2017.
 */
@Entity
public class Exchange {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "productId")
    private  Product product;

    @ManyToOne
    @JoinColumn(name = "userReceiver")
    private  Users userReceiver;

    @ManyToOne
    @JoinColumn(name = "userGiver")
    private  Users userGiver;

    private Date datecreated;

    private StatusExchange statusExchange;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Users getUserReceiver() {
        return userReceiver;
    }

    public void setUserReceiver(Users userReceiver) {
        this.userReceiver = userReceiver;
    }

    public Users getUserGiver() {
        return userGiver;
    }

    public void setUserGiver(Users userGiver) {
        this.userGiver = userGiver;
    }

    public Date getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }

    public StatusExchange getStatusExchange() {
        return statusExchange;
    }

    public void setStatusExchange(StatusExchange statusExchange) {
        this.statusExchange = statusExchange;
    }
}
