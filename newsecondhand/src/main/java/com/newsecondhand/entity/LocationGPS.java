package com.newsecondhand.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;


/**
 * Created by Bi on 4/9/2017.
 */
@Entity
public class LocationGPS {
    @Id
    @GeneratedValue
    private Long id;

    private double latitude;

    private double longtitude;

    private String address;

    private Date dateCreated;

    @OneToMany(mappedBy = "location", cascade = CascadeType.ALL)
    @JsonBackReference(value = "product")
    private Set<Product> product;

    @ManyToOne
    @JoinColumn(name = "usercreated")
    @JsonBackReference(value = "users")

    private  Users userCreated;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Set<Product> getProduct() {
        return product;
    }

    public void setProduct(Set<Product> product) {
        this.product = product;
    }

    public Users getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(Users userCreated) {
        this.userCreated = userCreated;
    }
}
