package com.newsecondhand.controller;

import com.newsecondhand.entity.Categories;
import com.newsecondhand.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Bi on 5/9/2017.
 */
@RestController
@RequestMapping(value = "/api/categories")
public class CategoriesController {

    @Autowired
    private CategoriesService categoriesService;

    @CrossOrigin
    @GetMapping
    public ResponseEntity getAllCategories(){
        return new ResponseEntity<>(categoriesService.getListCategories(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/{id}")
    public ResponseEntity getCategoryById(@PathVariable("id") long categoryid) {
        return new ResponseEntity<>(categoriesService.findCategoryById(categoryid), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping()
    public ResponseEntity createNewBook(@Validated @RequestBody Categories categories) {
        Categories todo = categoriesService.createNewCategory(categories);
        return new ResponseEntity<>(todo, HttpStatus.CREATED);
    }


    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteTodo(@PathVariable("id")  long id) {
        categoriesService.deleteCategory(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
