package com.newsecondhand.controller;

import com.newsecondhand.entity.Product;
import com.newsecondhand.repository.ProductRepository;
import com.newsecondhand.service.CategoriesService;
import com.newsecondhand.service.ProductService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Bi on 5/9/2017.
 */
@RestController
@RequestMapping(value = "/api/product")
public class ProductController {


    @Autowired
    private ProductService productService;

    @CrossOrigin
    @GetMapping
    public ResponseEntity getAllProducts(){
        return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
    }


    @CrossOrigin
    @GetMapping(value = "/{id}")
    public ResponseEntity getProductsById(@PathVariable("id") long productId){
        return new ResponseEntity<>(productService.findProductById(productId), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/location/{id}")
    public ResponseEntity getProductListByLocation(@PathVariable("id") long locationId ) {
        return new ResponseEntity<>(productService.getProductsByLocations(locationId), HttpStatus.OK);
    }

      @CrossOrigin
    @GetMapping(value = "/usercreated/{username}")
    public ResponseEntity getProductListByUserCreated(@PathVariable("username") String username ) {
        return new ResponseEntity<>(productService.getProductsByUserCreated(username), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/userreceiver/{username}")
    public ResponseEntity getProductListByUserReceiver(@PathVariable("username") String username ) {
        return new ResponseEntity<>(productService.getProductsByUserReceiver(username), HttpStatus.OK);
    }



    @CrossOrigin
    @GetMapping(value = "/categories/{id}/page/{pageid}")
    public ResponseEntity getByCategoriesWithPaging(@PathVariable("id") long categoryid, @PathVariable("pageid") int pageid) {
        return new ResponseEntity<>(productService.getListProductsByCategoryIdWithPaging(categoryid, pageid), HttpStatus.OK);
    }

    @CrossOrigin

    @GetMapping(value = "/categories/countpage/{id}")
    public ResponseEntity countPageOfListBooksByCategoryId(@PathVariable("id") long categoryid) {
        return new ResponseEntity<>(productService.getTotalPageOfProductsByCategoryWithPaging(  categoryid), HttpStatus.OK);
    }


    @CrossOrigin
    @GetMapping(value = "/new/page/{pageid}")
    public ResponseEntity getByCategoriesWithPaging(@PathVariable("pageid") int pageid) {
        return new ResponseEntity<>(productService.getListNewProductsWithPaging( pageid), HttpStatus.OK);
    }

    @CrossOrigin

    @GetMapping(value = "new/countpage/")
    public ResponseEntity countPageOfListBooksByCategoryId() {
        return new ResponseEntity<>(productService.getTotalPageOfNewProductsWithPaging(), HttpStatus.OK);
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> uploadFile(@RequestParam("file") MultipartFile file){
        try {
            File convertFile = new File("D:\\Backend-Newsecondhand\\backend-newsecondhand\\newsecondhand\\images\\"+file.getOriginalFilename());
            convertFile.createNewFile();
            FileOutputStream fout = new FileOutputStream(convertFile);
            fout.write(file.getBytes());
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<Object>("Upload successfully", HttpStatus.OK);
    }


//    @CrossOrigin
//    @RequestMapping(method = RequestMethod.POST)
//    public ResponseEntity createNewBook(@RequestParam("newProduct") String newProduct, @RequestPart("file") MultipartFile file,
//                                        RedirectAttributes redirectAttributes) throws Exception {
//        Product todo = productService.createNewProduct(newProduct, file);
//        redirectAttributes.addFlashAttribute("message",
//                "You successfully uploaded " + file.getOriginalFilename() + "!");
//        return new ResponseEntity<>(todo, HttpStatus.CREATED);
//    }

//    @CrossOrigin
//    @PatchMapping()
//    public ResponseEntity updateAmount(@Validated @RequestBody String json) {
//        JSONObject jsonObject = new JSONObject(json);
//        JSONObject productDTO = jsonObject.getJSONObject("bookDTO");
//        int amount = jsonObject.getInt("amountBook");
//        System.out.println(amount);
//        String todo = productService.updateAmount(bookDTO.getLong("bookId"), bookDTO.getInt("onBorrow"), bookDTO.getInt("inStock"), amount);
//        return new ResponseEntity<>(todo, HttpStatus.OK);
//    }
}
