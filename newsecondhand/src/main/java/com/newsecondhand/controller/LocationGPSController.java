package com.newsecondhand.controller;

import com.newsecondhand.dto.LocationDTO;
import com.newsecondhand.entity.Categories;
import com.newsecondhand.entity.LocationGPS;
import com.newsecondhand.service.CategoriesService;
import com.newsecondhand.service.LocationGPSSevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Bi on 17/9/2017.
 */
@RestController
@RequestMapping(value = "/api/location")
public class LocationGPSController {



    @Autowired
    private LocationGPSSevice locationGPSSevice;

    @CrossOrigin
    @GetMapping
    public ResponseEntity getAllLocation(){
        return new ResponseEntity<>(locationGPSSevice.getListLocationGPS(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/{id}")
    public ResponseEntity getLocationById(@PathVariable("id") long locationid) {
        return new ResponseEntity<>(locationGPSSevice.findLocationById(locationid), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/user/{username}")
    public ResponseEntity getLocationByUsername(@PathVariable("username") String username) {
        return new ResponseEntity<>(locationGPSSevice.getListLocationGPSByUsername(username), HttpStatus.OK);
    }



    @CrossOrigin
    @PostMapping()
    public ResponseEntity createNewLocation(@Validated @RequestBody LocationGPS item) {
        LocationGPS todo = locationGPSSevice.createNewLocation(item);
        return new ResponseEntity<>(todo, HttpStatus.CREATED);
    }


    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteLocation(@PathVariable("id")  long id) {
        locationGPSSevice.deleteLocation(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
