package com.newsecondhand.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Bi on 5/9/2017.
 */
@Controller
public class RouteController {
    @GetMapping(value = {"/", "/newsecondhand","/404"})
    public String indexPage() {
        return "index";
    }
}