package com.newsecondhand.controller;

import com.newsecondhand.common.CurrentDate;
import com.newsecondhand.dto.ExchangeDTO;
import com.newsecondhand.entity.Exchange;
import com.newsecondhand.repository.ExchangeRepository;
import com.newsecondhand.service.ExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;


/**
 * Created by Bi on 9/12/2017.
 */
@RestController
@RequestMapping(value = "/api/exchange")
public class ExchangeController {

    @Autowired
    private ExchangeService exchangeService;

    @CrossOrigin
    @PostMapping(value = "/new/{idproduct}/{usergiver}")
    public ResponseEntity createNewExchange(@PathVariable("idproduct") Long idproduct,@PathVariable("usergiver") String usergiver) {
        ExchangeDTO itemDTO = new ExchangeDTO();
        itemDTO.setIdProduct(idproduct);
        itemDTO.setUserGiver(usergiver);
        java.util.Date utilDate = new java.util.Date();
        itemDTO.setDatecreated(CurrentDate.getCurrentSQLDate());
        ExchangeDTO itemNew = exchangeService.createExchange(itemDTO);
        return new ResponseEntity<>(itemNew, HttpStatus.OK);
    }


    @CrossOrigin
    @PostMapping(value = "/completed/{idproduct}")
    public ResponseEntity completedExchange(@PathVariable("idproduct") Long idproduct) {
        ExchangeDTO itemNew = null;
        if(exchangeService.isExistPendingExchangeByProductId(idproduct)){
            itemNew = exchangeService.completeExchange(idproduct);
        }
        return new ResponseEntity<>(itemNew, HttpStatus.OK);
    }


    @CrossOrigin
    @PostMapping(value = "/cancelled/{idproduct}")
    public ResponseEntity cancelledExchange(@PathVariable("idproduct") Long idproduct) {
        ExchangeDTO itemNew = null;
        if(exchangeService.isExistPendingExchangeByProductId(idproduct)){
            itemNew = exchangeService.canceledExchange(idproduct);
        }
        return new ResponseEntity<>(itemNew, HttpStatus.OK);
    }
}
