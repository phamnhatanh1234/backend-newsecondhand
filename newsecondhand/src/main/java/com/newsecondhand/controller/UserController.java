package com.newsecondhand.controller;

import com.newsecondhand.common.ErrorException;
import com.newsecondhand.common.ErrorResponse;
import com.newsecondhand.dto.UserDTO;
import com.newsecondhand.entity.Users;
import com.newsecondhand.repository.UsersRepository;
import com.newsecondhand.service.CategoriesService;
import com.newsecondhand.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Bi on 7/9/2017.
 */
@RestController
@RequestMapping(value = "/api/user")
public class UserController  {

    @Autowired
    private UsersService usersService;

    @CrossOrigin
    @GetMapping
    public ResponseEntity getAllUsers(){
        return new ResponseEntity<>(usersService.getListUsers(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/{username}")
    public ResponseEntity getUsersByUsername(@PathVariable("username") String username) {
        return new ResponseEntity<>(usersService.findUserByUsername(username), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping()
    public ResponseEntity createNewUser(@Validated @RequestBody Users users) throws ErrorException {

        UserDTO todo = usersService.createNewUsers(users);
        if(todo == null) {
            throw new ErrorException("Thêm tài khoản không thành công!");
        }else{
            return new ResponseEntity<>(todo, HttpStatus.CREATED);
        }
    }


    @CrossOrigin
    @PutMapping(value = "/{username}")
    public ResponseEntity updateNewUser(@PathVariable("username") String username,@Validated @RequestBody Users users) throws ErrorException {
        if (usersService.findUserByUsername(username) == null) {
            throw new ErrorException("Tài khoản này không tồn tại");
        }
        if (!username.equals(users.getUsername())){
            throw new ErrorException("Chỉ được phép cập nhật thông tin tài khoản cá nhân");
        }
        UserDTO todo = usersService.updateUser(username, users);
        return new ResponseEntity<>(todo, HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping(value = "/{username}")
    public ResponseEntity deleteTodo(@PathVariable("username")  String username) throws ErrorException {
        if (usersService.findUserByUsername(username) == null ) {
            throw new ErrorException("Tài khoản này không tồn tại");
        }
        usersService.deleteUser(username);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(ErrorException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }
}
