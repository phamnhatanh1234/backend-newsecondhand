package Utils;


import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by Bi on 21/11/2017.
 */
public class ImageService {

    //dHeight=900
    public static BufferedImage resizeImageScalr(BufferedImage originalImage, int dHeight) {
        int dWidth = (dHeight*(originalImage.getWidth())/originalImage.getHeight());
        return Scalr.resize(originalImage, dWidth, dHeight);
    }

    //dWidth=657
    //dHeight=900
    public static BufferedImage autoCropImageScalr(BufferedImage originalImage, int dWidth, int dHeight) {
        int startX = 0;
        int startY = 0;
        if(originalImage.getWidth()>dWidth){
            startX=(originalImage.getWidth()-dWidth)/2;
            if(originalImage.getWidth()<startX+dWidth){
                dWidth=originalImage.getWidth()-startX;
            }
        }
        else{
            dWidth=originalImage.getWidth();
        }
        return Scalr.crop(originalImage, startX, startY, dWidth, dHeight);
    }

}
