import { Component, OnInit } from '@angular/core'
import { Subscription } from 'rxjs'
import { Router, ActivatedRoute } from '@angular/router'
import { ProductService } from '../_services/product.service'

@Component({
	selector: 'product-detail',
	templateUrl: 'product-detail.component.html',
	styleUrls:['./product-detail.scss']
})
export class ProductDetailComponent implements OnInit{
	public _id: number;
	public subscription: Subscription;
	public product: any;
	lat: number;
	lng: number;

	constructor(private router: Router, private activatedRoute: ActivatedRoute, private productService: ProductService) {
		// code...
	}

	ngOnInit() {
		this.subscription = this.activatedRoute.params.subscribe(params => {
			this._id = params['id'];
		});
		this.productService.GetSingle(this._id).subscribe((data) => {
			this.product = data;
			console.log(this.product.latitude + "; " + this.product.longtitude);
			this.lat = this.product.latitude;
			this.lng = this.product.longtitude;
		});
		// this.getUserLocation()
	}

	private getUserLocation() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(position => {
				this.lat = position.coords.latitude;
				this.lng = position.coords.longitude;
			});
		}
	}
	public Update(){
		alert("Help");
	}
}