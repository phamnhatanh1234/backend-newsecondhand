import { Component, OnInit } from '@angular/core'
import { Marker } from '../_model/marker'

@Component({
	selector: 'my-map',
	templateUrl: './map.component.html',
	styleUrls: ['./map.scss']
})
export class MapComponent implements OnInit{
	lat: number;
	lng: number;
	label: string;
	draggable: boolean;	markers: Marker[] = [

	];

	ngOnInit() {
		this.setCurrentPosition();
	}

	private setCurrentPosition() {
		if ("geolocation" in navigator) {
			navigator.geolocation.getCurrentPosition((position) => {
				this.lat = position.coords.latitude;
				this.lng = position.coords.longitude;
			});
		}
	}

	clickedMarker(label: string, index: number) {
		console.log(`clicked the marker: ${label || index}`)
	}

	mapClicked($event: any) {
		this.markers.push({
			lat: $event.coords.lat,
			lng: $event.coords.lng,
			label: '',
			draggable: true
		});
	}

	markerDragEnd(m: Marker, $event: any) {
		console.log('dragEnd', m, $event);
	}
}