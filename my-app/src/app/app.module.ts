import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { CropperSettings, ImageCropperComponent } from 'ng2-img-cropper';
import { MomentModule } from 'angular2-moment';
import { ImageUploadModule } from 'angular2-image-upload'

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NavbarLoginComponent } from './login-navbar/navbar-login.component';
import { FooterComponent } from './footer/footer.component';
import { CrComponent } from './cut-resize/cr.component';
import { NotFoundComponent } from './not-found/notfound.component';
import { AddAddressComponent } from './add-address/addAddress.component';
import { ProfileComponent } from './profile/profile.component';
import { AddProductComponent } from './add-product/addProduct.component';
import { MapComponent } from './map/map.component';

import { appRoutes } from './app.routes';
import { CategoriesService } from  './_services/categories.service';
import { ProductService } from './_services/product.service';
import { LocationService } from './_services/location.service';
import { UserService } from './_services/user.service';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProductDetailComponent,
    NavbarComponent,
    NavbarLoginComponent,
    FooterComponent,
    ImageCropperComponent,
    CrComponent,
    NotFoundComponent,
    AddAddressComponent,
    ProfileComponent,
    AddProductComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CommonModule,
    AgmCoreModule,
    MomentModule,
    appRoutes,
    ImageUploadModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCF-XGMl1XmcSaYt2VbCYnimFDRC2wsEus',
      libraries: ["places"]
    }),
    ReactiveFormsModule
  ],
  providers: [
  	CategoriesService,
    LocationService,
    ProductService,
    UserService,    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }