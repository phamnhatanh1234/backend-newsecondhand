import { Component, OnInit, Input } from '@angular/core'
import { UserService } from '../_services/user.service'
import { User } from '../_model/user'

@Component({
	selector: 'my-register',
	templateUrl: './register.component.html',
	styleUrls:['./register.scss']
})
export class RegisterComponent implements OnInit {

	constructor(private userService: UserService) {

	}

	@Input() user:User;
    responseStatus:Object= [];
    status:boolean ;

	ngOnInit() {
		this.user = new User();
	}

	submitPost() {
		console.log("submit Post click happend " + this.user.fullname)
        this.userService.createUser(this.user).subscribe(
           data => console.log(this.responseStatus = data),
           err => console.log(err),
           () => alert('Request Completed')
        ); 
        this.status = true;
	}
}