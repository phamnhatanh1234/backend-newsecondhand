import { Component, OnInit } from '@angular/core'
import { Subscription } from 'rxjs'
import { Observable } from 'rxjs/Rx'
import { Router, ActivatedRoute } from '@angular/router'
import { ProductService } from '../_services/product.service'
import * as moment from 'moment';
@Component({
	selector: 'my-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.scss']
})
export class HomeComponent {
	public products: any[];
	public pages: number[];
	public whatTime: any;

	constructor(private productService: ProductService, private router: Router, private activatedRoute: ActivatedRoute) { }

	ngOnInit() {
		moment.lang('vi');
		this.loadData();
		this.whatTime = new Date();
	}

	loadData() {
		this.productService.getList().subscribe((response: any) => {
			this.products = response;
		});
	}
}