import { Routes, RouterModule } from '@angular/router'
import { LoginComponent } from './login/login.component'
import { RegisterComponent } from './register/register.component'
import { ProductDetailComponent } from './product-detail/product-detail.component'
import { HomeComponent } from './home/home.component'
import { NavbarComponent } from './navbar/navbar.component'
import { NotFoundComponent } from './not-found/notfound.component'
import { AddAddressComponent } from './add-address/addAddress.component'
import { IndexComponent } from './index/index.component'
import { ProfileComponent } from './profile/profile.component'
import { AddProductComponent } from './add-product/addProduct.component'
import { MapComponent } from './map/map.component'

const routing: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'register', component: RegisterComponent },
	{ path: 'add-local', component: AddAddressComponent },
	{ path: 'product/:id', component: ProductDetailComponent },
	{ path: 'profile', component: ProfileComponent },
	{ path: 'add-product', component: AddProductComponent },
	{ path: 'map', component: MapComponent },
	{ path: '**', component: NotFoundComponent },
	// { path: 'categories/:id', component: ProductByCategoriesComponent },
]

export const appRoutes = RouterModule.forRoot(routing);