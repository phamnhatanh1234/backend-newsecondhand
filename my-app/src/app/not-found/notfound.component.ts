import { Component } from '@angular/core'

@Component({
	selector: 'not-found',
	templateUrl: './notfound.component.html',
	styleUrls: ['./notfound.scss']
})
export class NotFoundComponent {
	
}