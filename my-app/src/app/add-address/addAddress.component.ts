import { Component, OnInit, ElementRef, NgZone, ViewChild, Input } from '@angular/core'
import { FormsModule, FormControl, ReactiveFormsModule } from '@angular/forms'
import { } from 'googlemaps'
import { AgmCoreModule, MapsAPILoader } from '@agm/core'
import { Marker } from '../_model/marker'
import { LocationService } from '../_services/location.service'
import { Location } from '../_model/location'

@Component({
	selector: 'add-address',
	templateUrl: './addAddress.component.html',
	styleUrls: ['./addAddress.scss']
})
export class AddAddressComponent implements OnInit {
	public searchControl: FormControl;
	public latitude: number;
	public longitude: number;
	public zoom: number;
	public lat: number;
	public lng: number;
	markers: Marker[] = [];

	@ViewChild("search")
	public searchElementRef: ElementRef;

	constructor(private mapsAPILoader: MapsAPILoader, private ngZone: NgZone, private localService: LocationService) {

	}

	@Input() local: Location;
	responseStatus:Object= [];
	status:boolean ;

	ngOnInit() {
		this.zoom = 15;
		// this.latitude = 39.8282;
		// this.longitude = -98.5795;

		this.local = new Location();

		this.searchControl = new FormControl();

		this.setCurrentPosition();

		this.mapsAPILoader.load().then(() => {
			let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
				types: ["address"]
			});
			autocomplete.addListener("place_changed", () => {
				this.ngZone.run(() => {
					let place: google.maps.places.PlaceResult = autocomplete.getPlace();

					if (place.geometry === undefined || place.geometry === null) {
						return;
					}

					this.latitude = place.geometry.location.lat();
					this.longitude = place.geometry.location.lng();
					this.zoom = 15;
				});
			});
		});
	}

	private setCurrentPosition() {
		if ("geolocation" in navigator) {
			navigator.geolocation.getCurrentPosition((position) => {
				this.latitude = position.coords.latitude;
				this.longitude = position.coords.longitude;
				this.zoom = 15;
			});
		}
	}

	// clickedMarker(label: string, index: number) {
	// 	console.log(`clicked the marker: ${label || index}`)
	// }

	mapClicked($event: any) {
		this.mapsAPILoader.load().then(() => {
			this.latitude= $event.coords.lat;
			this.longitude= $event.coords.lng;
		})	
	}

	markerDragEnd(m: Marker, $event: MouseEvent) {
		console.log('dragEnd', m, $event);
	}

	submitLocation(search: string, lat: number, lng: number, user: string) {
		this.local.address = search + " ";
		this.local.latitude = lat;
		this.local.longtitude = lng;
		this.local.userCreated = "phamnhatanh";
		console.log(this.local);
		this.localService.createLocal(this.local).subscribe(
			data => console.log(this.responseStatus = data),
			err => console.log("ERROR!!!" + err),
			() => console.log('Request Completed')
			); 
		this.status = true;
	}
}