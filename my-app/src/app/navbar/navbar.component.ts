import { Component, OnInit } from '@angular/core'
import { Observable } from 'rxjs/Rx'
import { Subscription } from 'rxjs'
import { CategoriesService } from '../_services/categories.service'

@Component({
	selector: 'my-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.scss']
})
export class NavbarComponent implements OnInit {
	public categories: any[];
	public icon: string;

	constructor(private categoryService: CategoriesService) {

	}

	ngOnInit() {
		this.loadData();
	}

	loadData() {
		this.categoryService.getList().subscribe((response: any) => {
			this.categories = response;
			// console.log(this.categories)
		});
	}
}