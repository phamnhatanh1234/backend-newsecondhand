import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Location } from '../_model/location'

@Injectable()
export class LocationService {
	private apiUrl = "http://localhost:8081/api/location";
	http: Http;
    returnCommentStatus:Object = [];

	constructor(private _http: Http) {
		this.http = _http;
	}

	getList(): Observable<any[]> {
		return this.http.get(this.apiUrl).map((response: Response) => response.json());
	}

	getSingle(id: number): Observable<any> {
		return this.http.get(this.apiUrl+"/"+id).map((response: Response) => response.json());
	}

	createLocal(location: Location) {
        return this.http.post(this.apiUrl, location).map((response: Response) => response.json());
    }
}