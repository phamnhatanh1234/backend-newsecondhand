import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { User } from '../_model/user'

@Injectable()
export class UserService {
	private apiUrl = "http://localhost:8081/api/user";
	http: Http;
    returnCommentStatus:Object = [];
	
	constructor(private _http: Http) {
		this.http = _http;
	}

    createUser(user: User) {
        return this.http.post(this.apiUrl, user).map((response: Response) => response.json());
    }
}