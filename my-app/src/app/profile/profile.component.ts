import { Component, OnInit } from '@angular/core'
import * as moment from 'moment';

@Component({
	selector: 'my-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.scss']
})
export class ProfileComponent {
	public whatTime: any;

	ngOnInit() {
		moment.lang('vi');
		this.whatTime = new Date();
	}
}